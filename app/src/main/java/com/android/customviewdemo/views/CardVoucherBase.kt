package com.android.customviewdemo.views

import android.content.Context
import android.content.res.Resources
import android.graphics.Paint
import android.graphics.Path
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.core.content.ContextCompat
import com.android.customviewdemo.R

open class CardVoucherBase @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : LinearLayoutCompat(context, attrs, defStyle) {

    private val Int.dp: Int get() = (this * Resources.getSystem().displayMetrics.density + 0.5f).toInt()

    protected var radius: Float = 25f // radius card
    protected var circleBottomPercent: Float = 29f // percent bottom position arc
    protected var circleRadius: Float = 25f // arc left and right radius
    protected val shadowColor = ContextCompat.getColor(context, R.color.loyalty_card_shadow_start_color)
    protected var shadowHeight: Int = 6.dp // card shadow height
    protected val paintStrokeWidth: Float = 2f

    protected var path: Path = Path() // main path
    protected var paint: Paint = Paint() // main paint

    init {
        initialize(attrs)
    }

    protected open fun initialize(attrs: AttributeSet? = null){}

    /**
     * added view child, set child padding from shadow
     */
    override fun addView(child: View?, params: ViewGroup.LayoutParams?) {
        if (params != null && params is MarginLayoutParams) {
            val padding = shadowHeight // height shadow

            val leftMargin = when {
                params.marginStart > 0 -> params.marginStart + padding
                params.leftMargin > 0 -> params.leftMargin + padding
                else -> 0
            }

            var topMargin = params.topMargin
            if (topMargin > 0) topMargin += padding

            val rightMargin = when {
                params.rightMargin > 0 -> params.rightMargin + padding
                params.marginEnd > 0 -> params.marginEnd + padding
                else -> 0
            }

            var bottomMargin = params.bottomMargin
            if (bottomMargin > 0) bottomMargin += padding

            params.setMargins(leftMargin, topMargin, rightMargin, bottomMargin)
            params.marginStart = leftMargin
            params.marginEnd = rightMargin
        }

        super.addView(child, params)
    }

    /**
     * return bottom notch position (from percent to point)
     */
    protected fun getCenterCircle(height: Float): Float = height - circleBottomPercent - circleRadius

}