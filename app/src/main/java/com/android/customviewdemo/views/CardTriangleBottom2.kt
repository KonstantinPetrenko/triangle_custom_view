package com.android.customviewdemo.views

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import com.android.customviewdemo.R


class CardTriangleBottom2 : CardVoucherBase {
    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet? = null) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet? = null, defStyle: Int = 0) : super(
        context,
        attrs,
        defStyle
    )

    private val triangleCount = 17 // count triangles

    override fun initialize(attrs: AttributeSet?) {
        // read parameters
        if (attrs != null) {
            val a = context.obtainStyledAttributes(attrs, R.styleable.CardTriangleBottom2)
            radius = a.getDimensionPixelSize(R.styleable.CardTriangleBottom2_cardTriangleRadius, 25)
                .toFloat()
            a.recycle()
        }

        // prepare paint
        paint.apply {
            strokeWidth = 2f
            color = Color.WHITE
            isAntiAlias = true
            isDither = true

            setShadowLayer(12f, 0f, 2f, shadowColor)
            style = Paint.Style.FILL

            strokeJoin = Paint.Join.ROUND
            strokeCap = Paint.Cap.ROUND
        }
        setLayerType(LAYER_TYPE_SOFTWARE, paint) // for shadow
        setWillNotDraw(false) // for draw canvas
    }

    /**
     * draw bottom triangles
     */
    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        drawCard(canvas)
    }

    /**
     * draw
     */
    private fun drawCard(canvas: Canvas) {
        val corners = floatArrayOf(radius, radius, radius, radius, 0f, 0f, 0f, 0f)
        val width = canvas.width.toFloat() - (shadowHeight * 2) // shadow left + right
        val height = canvas.height.toFloat() - (shadowHeight * 2) // shadow top + bottom
        // width one triangle
        val widthTriangle = width / triangleCount
        // height triangle
        val heightTriangle = widthTriangle / 2f
        // shadow triangles top + bottom
        val heightBottom = height - heightTriangle - shadowHeight * 2

        // draw card
        path.apply {
            reset()
            moveTo(0f + shadowHeight, heightBottom + shadowHeight) // to begin coordinate bottom

            for (i in 0..triangleCount) { // draw triangles...
                var x = widthTriangle * i // x coordinate
                // to up center triangle
                lineTo(x + shadowHeight, heightBottom + heightTriangle + shadowHeight)
                // from upping to bottom with triangle
                val tmpX = x + heightTriangle + shadowHeight
                x = if (tmpX < width + shadowHeight) tmpX else width + shadowHeight
                // draw line to bottom
                lineTo(x, heightBottom + shadowHeight)
            }
            // main round rectangle with upper corners
            val rect = RectF(
                /*left*/ shadowHeight.toFloat(),
                /*top*/ shadowHeight.toFloat(),
                /*right*/width + shadowHeight,
                /*bottom*/heightBottom + shadowHeight
            )
            addRoundRect(rect, corners, Path.Direction.CW)
        }
        // draw with paint
        canvas.drawPath(path, paint)
    }

}